FROM node:10.24.0-alpine3.11
WORKDIR /app
COPY . .
RUN npm install
CMD ["npm", "start"]